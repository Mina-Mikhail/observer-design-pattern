/*
 * *
 *  * Created by MIna Mikhail on 2/25/19 3:07 PM
 *  * Copyright (c) 2019 . All rights reserved.
 *  * Last modified 2/25/19 3:07 PM
 *
 */

package codes.devslab.observer_pattern;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
  @Test
  public void addition_isCorrect() {
    assertEquals(4, 2 + 2);
  }
}