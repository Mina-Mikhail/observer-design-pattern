/*
 * *
 *  * Created by MIna Mikhail on 2/25/19 3:07 PM
 *  * Copyright (c) 2019 . All rights reserved.
 *  * Last modified 2/25/19 2:57 PM
 *
 */

package codes.devslab.observer_pattern;

import android.os.Handler;
import codes.devslab.observer_pattern.data.entity.User;
import java.util.ArrayList;

public class UserDataRepository
    implements Subject {

  private User user;
  private static UserDataRepository INSTANCE = null;

  private ArrayList<RepositoryObserver> mObservers;

  private UserDataRepository() {
    mObservers = new ArrayList<>();
    getNewDataFromRemote();
  }

  // Creates a Singleton of the class
  public static UserDataRepository getInstance() {
    if (INSTANCE == null) {
      INSTANCE = new UserDataRepository();
    }
    return INSTANCE;
  }

  // Simulate network updates
  private void getNewDataFromRemote() {
    final Handler handler = new Handler();
    final Runnable runnable = new Runnable() {
      @Override
      public void run() {
        /* do what you need to do */
        sendUpdates();
        /* and here comes the "trick" */
        handler.postDelayed(this, 3000);
      }
    };
    handler.postDelayed(runnable, 3000);
  }

  @Override
  public void registerObserver(RepositoryObserver repositoryObserver) {
    if (repositoryObserver != null && !mObservers.contains(repositoryObserver)) {
      mObservers.add(repositoryObserver);
    }
  }

  @Override
  public void removeObserver(RepositoryObserver repositoryObserver) {
    if (repositoryObserver != null && mObservers.contains(repositoryObserver)) {
      mObservers.remove(repositoryObserver);
    }
  }

  @Override
  public boolean isSubscribed(RepositoryObserver repositoryObserver) {
    return repositoryObserver != null && mObservers.contains(repositoryObserver);
  }

  @Override
  public void notifyObservers() {
    for (RepositoryObserver ob : mObservers) ob.onUserDataChanged(user);
  }

  private void sendUpdates() {
    this.user = User.getRandomUser();
    notifyObservers();
  }
}