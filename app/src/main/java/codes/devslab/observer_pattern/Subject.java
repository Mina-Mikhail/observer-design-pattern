/*
 * *
 *  * Created by MIna Mikhail on 2/25/19 3:07 PM
 *  * Copyright (c) 2019 . All rights reserved.
 *  * Last modified 2/25/19 2:57 PM
 *
 */

package codes.devslab.observer_pattern;

public interface Subject {

  void registerObserver(RepositoryObserver repositoryObserver);

  void removeObserver(RepositoryObserver repositoryObserver);

  boolean isSubscribed(RepositoryObserver repositoryObserver);

  void notifyObservers();
}
