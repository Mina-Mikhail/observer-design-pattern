/*
 * *
 *  * Created by MIna Mikhail on 2/25/19 3:07 PM
 *  * Copyright (c) 2019 . All rights reserved.
 *  * Last modified 2/25/19 2:57 PM
 *
 */

package codes.devslab.observer_pattern.data.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class User {

  private String name;
  private int age;

  private User(String name, int age) {
    this.name = name;
    this.age = age;
  }

  public String getName() {
    return name;
  }

  public int getAge() {
    return age;
  }

  public static User getRandomUser() {
    List<User> randomUsers = new ArrayList<>();
    randomUsers.add(new User("Mina Mikhail", 24));
    randomUsers.add(new User("Mohamed Ali", 20));
    randomUsers.add(new User("Ahmed Adel", 70));
    randomUsers.add(new User("John Doe", 101));
    randomUsers.add(new User("Mohamed Salah", 43));
    randomUsers.add(new User("Steve Jobs", 66));

    Random r = new Random();
    int lowRange = 0;
    int highRange = randomUsers.size() - 1;
    int result = r.nextInt(highRange - lowRange) + lowRange;

    return randomUsers.get(result);
  }
}
