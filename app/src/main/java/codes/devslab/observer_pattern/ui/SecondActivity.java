/*
 * *
 *  * Created by MIna Mikhail on 2/25/19 3:07 PM
 *  * Copyright (c) 2019 . All rights reserved.
 *  * Last modified 2/25/19 3:05 PM
 *
 */

package codes.devslab.observer_pattern.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import codes.devslab.observer_pattern.R;

public class SecondActivity
    extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_second);

    setUpViews();
  }

  private void setUpViews() {
    setUpToolbar();
  }

  private void setUpToolbar() {
    if (getSupportActionBar() != null) {
      setTitle("Second Activity");

      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      getSupportActionBar().setDisplayShowHomeEnabled(true);
    }
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        onBackPressed(); // Handle back button in toolbar
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }
}