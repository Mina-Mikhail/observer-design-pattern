/*
 * *
 *  * Created by MIna Mikhail on 2/25/19 3:07 PM
 *  * Copyright (c) 2019 . All rights reserved.
 *  * Last modified 2/25/19 2:57 PM
 *
 */

package codes.devslab.observer_pattern.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import codes.devslab.observer_pattern.R;
import codes.devslab.observer_pattern.RepositoryObserver;
import codes.devslab.observer_pattern.Subject;
import codes.devslab.observer_pattern.UserDataRepository;
import codes.devslab.observer_pattern.data.entity.User;

public class MainActivity
    extends AppCompatActivity
    implements RepositoryObserver {

  private Subject mUserDataRepository;

  private Button btnStartUpdates;
  private Button btnStopUpdates;
  private Button btnOpenSecondActivity;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    setUpViews();
    handleClicks();
  }

  private void setUpViews() {
    btnStartUpdates = findViewById(R.id.btn_start_updates);
    btnStopUpdates = findViewById(R.id.btn_stop_updates);
    btnOpenSecondActivity = findViewById(R.id.btn_open_second_activity);
  }

  private void handleClicks() {
    btnStartUpdates.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        startUpdates();
      }
    });

    btnStopUpdates.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        stopUpdates();
      }
    });

    btnOpenSecondActivity.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        startActivity(new Intent(MainActivity.this, SecondActivity.class));
      }
    });
  }

  private void startUpdates() {
    mUserDataRepository = UserDataRepository.getInstance();
    mUserDataRepository.registerObserver(MainActivity.this);

    Toast.makeText(this, "Updates Started Successfully", Toast.LENGTH_SHORT).show();
  }

  private void stopUpdates() {
    if (mUserDataRepository != null && mUserDataRepository.isSubscribed(MainActivity.this)) {
      mUserDataRepository.removeObserver(MainActivity.this);

      Toast.makeText(this, "Updates Stopped Successfully", Toast.LENGTH_SHORT).show();
    }
  }

  @Override
  public void onUserDataChanged(User user) {
    String message = "new update:"
        + "\n"
        + "Name: "
        + user.getName()
        + "\n"
        + "Age: "
        + user.getAge();

    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
  }

  @Override
  protected void onDestroy() {
    stopUpdates();
    super.onDestroy();
  }
}