/*
 * *
 *  * Created by MIna Mikhail on 2/25/19 3:07 PM
 *  * Copyright (c) 2019 . All rights reserved.
 *  * Last modified 2/25/19 2:57 PM
 *
 */

package codes.devslab.observer_pattern;

import codes.devslab.observer_pattern.data.entity.User;

public interface RepositoryObserver {

  void onUserDataChanged(User user);
}